require "test_helper"

class QuakeLogParserTest < Minitest::Test

	def setup
		@fixtures_path = "#{File.dirname(__FILE__)}/fixtures"
	end

  def test_that_it_has_a_version_number
    refute_nil QuakeLogParser::VERSION
  end

  def test_if_file_exists_and_is_readable
  	filename = 'logs/games.log'
		logParser = QuakeLogParser::LogParser.new(filename)

  	assert_equal true, logParser.file_exists
  end

  def test_if_not_file_exists
  	filename = 'logs/games2.log'
		logParser = QuakeLogParser::LogParser.new(filename)

  	assert_equal false, logParser.file_exists
  end

  def test_if_file_is_not_readable
  	filename = "#{@fixtures_path}/no_lines"
  	logParser = QuakeLogParser::LogParser.new(filename)

  	assert_equal 'No line in the file', logParser.read_file
  end

  def test_convert_lines_in_array
  	filename = 'logs/games.log'
		logParser = QuakeLogParser::LogParser.new(filename)
		lines = logParser.read_file

		assert_instance_of Array, lines
  end

  def test_parse_game
  	filename = "#{@fixtures_path}/new_game"
  	logParser = QuakeLogParser::LogParser.new(filename)
  	
  	assert_instance_of Array, logParser.parse_game
  end

  def test_parse_game_lines_players
    filename = "#{@fixtures_path}/new_game"
    logParser = QuakeLogParser::LogParser.new(filename)
    games = logParser.parse_game
    game = logParser.parse_game_lines(games[0])

    assert_instance_of QuakeLogParser::Player, game.players["Isgalamido"]
  end

  def test_parse_game_lines_kills
    filename = "#{@fixtures_path}/new_game_with_kill"
    logParser = QuakeLogParser::LogParser.new(filename)
    games = logParser.parse_game
    game = logParser.parse_game_lines(games[0])

    assert_instance_of QuakeLogParser::Kill, game.kills[0]
  end
  
end