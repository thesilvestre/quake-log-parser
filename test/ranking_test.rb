require "test_helper"
require_relative "../lib/quake_log_parser/ranking"

class RankingTest < Minitest::Test

	def setup
		@fixtures_path = "#{File.dirname(__FILE__)}/fixtures"
	end

	def test_create_ranking_response
		filename = "logs/games.log"
		logParser = QuakeLogParser::LogParser.new(filename)
		games = logParser.parse_game
		
		games.each do |game|
			game = logParser.parse_game_lines(game)
			game.kills.each	do |kill|
				game.create_score_game(kill)
			end
		end

		ranking = QuakeLogParser::Ranking.new(games)
		ranking_hashes = ranking.get_players
		assert_equal 178, ranking.create(ranking_hashes)["Isgalamido"][:kills]
		assert_equal 154, ranking.create(ranking_hashes)["Zeh"][:kills]
		assert_equal 132, ranking.create(ranking_hashes)["Oootsimo"][:kills]
	end

end