require "test_helper"

class GameTest < Minitest::Test

	def setup
		@fixtures_path = "#{File.dirname(__FILE__)}/fixtures"
	end

	def test_create_game
		filename = "#{@fixtures_path}/new_game"
  	logParser = QuakeLogParser::LogParser.new(filename)
  	
  	assert_instance_of QuakeLogParser::Game, logParser.parse_game[0]
	end
	
	def test_score_game_equal_zero
		filename = "#{@fixtures_path}/new_game"
		logParser = QuakeLogParser::LogParser.new(filename)
		games = logParser.parse_game
		game = logParser.parse_game_lines(games[0])

		assert_equal game.players["Isgalamido"].kills, 0
		assert_equal game.players["Isgalamido"].normal_deaths, 0
		assert_equal game.players["Isgalamido"].suicides, 0
	end

	def test_score_game_greater_zero
		filename = "logs/games.log"
		logParser = QuakeLogParser::LogParser.new(filename)
		games = logParser.parse_game
		game = logParser.parse_game_lines(games[1])

		game.kills.each	do |kill|
			game.create_score_game(kill)
		end

		assert_equal game.players["Isgalamido"].kills, 1
		assert_equal game.players["Isgalamido"].normal_deaths, 0
		assert_equal game.players["Isgalamido"].suicides, 10
	end

end