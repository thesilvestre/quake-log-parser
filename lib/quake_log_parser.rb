require_relative "quake_log_parser/version"
require_relative "quake_log_parser/parse_rule_pattern"
require_relative "quake_log_parser/game"
require_relative "quake_log_parser/player"
require_relative "quake_log_parser/kill"


module QuakeLogParser
  class LogParser
  	
  	def initialize(filename)
  		@filename = filename
  	end
  	
  	def file_exists
  		File.file?(@filename) and File.readable?(@filename)
  	end

  	def read_file
  		arrayLines = []
  		File.open(@filename, 'r').each do |line|
  			arrayLines << line
  		end

  		return 'No line in the file' if arrayLines.count == 0
  		arrayLines
  	end

  	def parse_game
  		#All the lines in log
  		lines = read_file
      games = []

  		initial_game_line = 0
  		final_game_line = 0
  		total_games = 0

  		lines.each_with_index do |line, index|
  			#Start game
  			if !line[ParseRulePattern.start_game].nil?
  				initial_game_line = index
  			elsif !line[ParseRulePattern.end_game].nil?
  				final_game_line = index
  				new_lines = lines[initial_game_line, final_game_line - initial_game_line]

  				#Instantiating a game
  				games << Game.new(new_lines, total_games)
  				
  				total_games += 1
		  	end
		  end

  		games
  	end

    def parse_game_lines(game)
      #Iterate each line in the game
      game.game_lines.each do |line|
        if !line[ParseRulePattern.player_connect].nil?
          #Get each player name
          player_name = line[ParseRulePattern.player_connect][ParseRulePattern.player_name]
          
          #Instantiating a player game
          game.players[player_name] = Player.new(player_name) unless game.players[player_name]
        elsif !line[ParseRulePattern.kill].nil?
          #In case the line be a kill report
          kill_line = line[ParseRulePattern.kill]
          
          #Instantiating a kill
          kill = Kill.new(kill_line[ParseRulePattern.killer], kill_line[ParseRulePattern.killed], kill_line[ParseRulePattern.death_reason])
          
          #Stoging all the deaths in object game
          game.kills << kill
        end
      end

      game
    end

  end
end
