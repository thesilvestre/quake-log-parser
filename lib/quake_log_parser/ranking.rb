module QuakeLogParser
	class Ranking
		
		def initialize(games)
			@games = games
			@players_hash = []
		end

		#Iterating games and players to increase kills and ordinate by score
		def get_players

      @players_hash = {}
      @games.each do |game|
      	#Iterating players in the game
        game.players.each do |player_name, player| 
        	#If player not added yet
          if @players_hash[player_name].nil?
            @players_hash[player_name] = Player.new(player.name, player.kills, player.normal_deaths, player.suicides)
          else
          	#If player already exists
            @players_hash[player_name].kills += player.kills
            @players_hash[player_name].normal_deaths += player.normal_deaths
            @players_hash[player_name].suicides += player.suicides
          end
          
        end

      end
      #Order by score
      players = @players_hash.sort_by { |k, v| v.score }.reverse!
      players.map { |player_array| player_array[1] }
		end

    def create(players_hash)
			ranking = {}
			players_hash.each do |player|
				#Generating hash of each player
				ranking[player.name] = player.to_hash
			end
			ranking
    end

	end
end