require 'json'

module QuakeLogParser
	class Game

		attr_accessor :name, :players, :start_time, :end_time, :game_lines, :kills
		
		def initialize(array_lines = [], id = 0)
			@id = id
      @name = "Game #{id+1}"
			@players = {}
      @kills = []
      @kills_by_means = {}
      @start_time = array_lines.first.match(ParseRulePattern.start_game_time)
			@end_time = array_lines.last.match(ParseRulePattern.end_game_time)
			@game_lines = array_lines
		end

		#Increment kills for each player
		def create_score_game(kill)
			if kill.killer == kill.killed or kill.killer == "<world>"
        @players[kill.killed].suicides += 1 
      else
        @players[kill.killer].kills += 1
        @players[kill.killed].normal_deaths += 1 
			end
		end

		def total_kills
			@kills.count
		end

		def to_hash
			{ 
				:start_time => @start_time, 
				:end_time => @end_time, 
				:total_kills => self.total_kills, 
				:players => @players.map { |player| player[0] },
				:kills => self.get_hash_players
			}
		end
		
		#Get hash to show details of player
		def get_hash_players
			kills = []
			@players.each do |name, player|
				kills << {
					name => player.to_hash
				}
			end
			kills
		end

		def to_s
			"#{@name} : #{JSON.pretty_generate(self.to_hash)}"
		end

	end
end