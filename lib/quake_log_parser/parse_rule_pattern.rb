module QuakeLogParser
	class ParseRulePattern

		def self.start_game
			/InitGame/
		end

		def self.end_game
			/ShutdownGame/
		end

		def self.start_game_time
			/(?<start_at>\d{1,2}:\d{2})/
		end

		def self.end_game_time
			/(?<end_at>\d{1,2}:\d{2})/
		end

		def self.player_connect
			/ClientUserinfoChanged: \d n\\(.*?)\\/
		end

		def self.player_name
			/(?<=\\)(.*?)(?=\\)/
		end

		def self.kill
			/:\s([^:]+)\skilled\s(.*?)\sby\s[a-zA-Z_]+/
		end

		def self.killer
			/(?<=:\s)(.*?)(?=\skilled)/
		end
	  
		def self.killed
			/(?<=killed\s)(.*?)(?=\sby)/
		end
	  
		def self.death_reason
			/(?<=by\s)(.*?)(?=$)/
		end

	end
end