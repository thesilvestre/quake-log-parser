module QuakeLogParser
	class Player

		attr_accessor :name, :kills, :normal_deaths, :suicides
		
		def initialize(name = 'player', kills = 0, normal_deaths = 0, suicides = 0)
	    @name = name
	    @kills = kills
	    @normal_deaths = normal_deaths
	    @suicides = suicides
		end

		def to_hash
			{
				:kills => @kills,
				:normal_deaths => @normal_deaths,
				:suicides => @suicides,
				:score => self.score
			}
		end

		def score
			@kills - @suicides
		end

	end
end