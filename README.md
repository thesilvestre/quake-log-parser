# QuakeLogParser

Welcome to QuakeLogParser! In this application, you'll find the files you need to be able to parse your quake 3 log file. The Ruby code is in the directory `lib/quake_log_parser`. 

This app was built in Ruby 2.4, using bundler gem and rake to automate the tests.

## Installation

After clone this repository, you should run bellow code in the directory of application:

And then execute:

    $ bundle install

## Usage

To run this application:

    $ ruby main.rb

    Or

    $ ruby main.rb path/to/log

To run the tests:

    $ rake

Obs: type `gem install rake` if you don't have it installed

## Output

The output on this application is a console response with a ranking players by score and a web page with an user friendly interface.
