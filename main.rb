require_relative "lib/quake_log_parser"
require_relative "lib/quake_log_parser/ranking"
require_relative "lib/quake_log_parser/helper"
require 'erb'

filename = ARGV[0] || 'logs/games.log'
#Instantiate a log class
logParser = QuakeLogParser::LogParser.new(filename)
#Check if file exists
if logParser.file_exists
	#Parsing game lines
	@games = logParser.parse_game

	puts "QUAKE 3 LOGER - LISTING GAMES\n\n"
	#Iterating games to parse their lines
	@games.each do |game|
		#Parsing inside game lines
		game = logParser.parse_game_lines(game)
		#Creating a score for each player
		game.kills.each	do |kill|
			game.create_score_game(kill)
		end

		puts game
	end

	puts "\n\nQUAKE 3 LOGER - LISTING RANKING OF PLAYER BY SCORE\n\n"
	#Creating a ranking by score
	ranking_obj = QuakeLogParser::Ranking.new(@games)
	ranking_hashes = ranking_obj.get_players
	@ranking = ranking_obj.create(ranking_hashes)
	puts JSON.pretty_generate(@ranking)
else
	puts 'No file found'
end

b = binding

template_index = File.open("views/index.html.erb", 'r').read
erb_template_index = ERB.new(template_index)
File.open("index.html", 'w+') { 
	|file| file.write(erb_template_index.result(b)) 
}

if QuakeLogParser::OS.mac?
	open_html = "open index.html"
	output = `#{open_html}`
elsif QuakeLogParser::OS.linux?
  open_html = "xdg-open index.html"
  output = `#{open_html}`
end